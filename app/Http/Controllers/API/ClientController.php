<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Products;
use Exception;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getProducts()
    {
        try {
            $data = Products::get();
            return response()->json(['status' => 'success', 'message' => 'ok', 'product'=>$data]);
        } catch(Exception $e) {
            return response()->json(['status' => 'error', 'message'=>$e->getMessage()]);
        }
    }

    public function getProductsByIds(Request $request)
    {
        try {

            $data = Products::whereIn('id', $request->ids)->get();
            return response()->json(['status' => 'success', 'message' => 'ok', 'product'=>$data]);
        } catch(Exception $e) {
            return response()->json(['status' => 'error', 'message'=>$e->getMessage()]);
        }
    }

    public function buyProducts(Request $request)
    {
        try {
            for($i =0; $i < count($request->products) ; $i++ ){
                $prod = Products::find($request->products[$i]['id']);
                $prod->stock = $prod->stock - $request->products[$i]['cantidad'];
                $prod->save();
            }
            return response()->json(['status' => 'success', 'message' => 'ok']);
        } catch(Exception $e) {
            return response()->json(['status' => 'error', 'message'=>$e->getMessage()]);
        }
    }

}
