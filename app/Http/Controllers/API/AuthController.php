<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Exception;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {

            $validate = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|max:255|unique:users',
                'password' => 'required',
                'age' => 'required',
                'gender' => 'required',
            ]);

            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->age = $request->age;
            $user->gender = $request->gender;
            $user->save();
            
            return response()->json(['status' => 'success', 'message' => 'ok', 'user'=>$user]);

        } catch(Exception $e) {
            return response()->json(['status' => 'error', 'message'=>$e->getMessage()]);
        }
    }

    public function login(Request $request)
    {
        try {
            $loginData = $request->validate([
                'email' => 'required',
                'password' => 'required'
            ]);
    
            if(!auth::attempt($loginData)){
                return response()->json(['status' => 'error', 'message'=> 'El usuario y/o contraseña son incorrectos']);
            }

            $user = User::where('email', $request->email)->first();

            DB::delete("delete from oauth_access_tokens WHERE user_id = $user->id");
            $accessToken = auth()->user()->createToken('authToken')->accessToken;            

            return response()->json(['status' => 'success', 'message'=> 'ok', 'user'=>auth()->user(), 'access_token' => $accessToken]);


        } catch(Exception $e) {
            return response()->json(['status' => 'error', 'message'=>$e->getMessage()]);
        }
    }

    public function getInfoByToken(Request $request)
    {
        try {
            $loginData = $request->validate([
                'access_token' => 'required'
            ]);

            $token = base64_decode($request->access_token);
            
            $arr = explode("}", $token);
            $arr[1] .= '}';
            $encodeData = json_decode($arr[1],true );

            $id_oat = $encodeData['jti'];
            // echo $id_oat;

            $data = DB::table('oauth_access_tokens')
                    ->join('users','users.id', '=', 'oauth_access_tokens.user_id')
                    ->where('oauth_access_tokens.id',$id_oat)
                    ->select('oauth_access_tokens.expires_at','users.id','users.name','users.email','users.age','users.gender')
                    ->first();

            $current = Carbon::now();
            // echo $data->expires_at . ' ------ '. $current;
            if($data && $current <= $data->expires_at)
                    return response()->json(['status' => 'success', 'message'=> 'ok', 'user'=>$data],200);
            

            return response()->json(['status' => 'error', 'message'=>'Tu token ha expirado']);
            

        } catch(Exception $e) {
            return response()->json(['status' => 'error', 'message'=>$e->getMessage()]);
        }
    }
}
