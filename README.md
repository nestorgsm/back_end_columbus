# back-end-columbus

API's para el proyecto de front-end para la prueba tecnica de Columbus

### Requisitos 📋

-   PHP 8.1
-   Laravel 9
-   Composer

---

### Configuración ⚙️️

Para configurar se requiere crear y configurar el archivo
`.env` basado en `.env.example`
Configurar la conexión a la BD, crear un base de datos llamda 'columbus'

---

### Instalación 🔌

```bash
git clone https://user@gitlab.com/nestorgsmf/back_end_columbus.git
cd back_end_columbus
composer install
php artisan migrate
php artisan passport:install
php artisan key:generate
```

---

### Iniciar servicio 🏃

```bash
php artisan serve
```

---

### Construido con 🛠️

-   Visual Studio Code
-   Passport

---

### Autor 🎎️

-   Néstor Sánchez
