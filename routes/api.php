<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [App\Http\Controllers\API\AuthController::class, 'register']);
Route::post('login', [App\Http\Controllers\API\AuthController::class, 'login']);

Route::post('getInfoByToken', [App\Http\Controllers\API\AuthController::class, 'getInfoByToken']);

Route::post('client/getProducts', [App\Http\Controllers\API\ClientController::class, 'getProducts']);
Route::post('client/getProductsByIds', [App\Http\Controllers\API\ClientController::class, 'getProductsByIds']);
Route::post('client/buyProducts', [App\Http\Controllers\API\ClientController::class, 'buyProducts']);